var app = angular.module('app', ['ui.grid','ui.grid.selection','cp.ngConfirm']);

app.controller('myCtrl', function ($scope,$filter,uiGridConstants,$http,$ngConfirm) {
    
    $scope.checkDataRes = 0;
    $scope.editing = 0;
    
    $scope.gridOptions = {
        enableRowSelection: true,
        enableSelectAll: true,
        showColumnFooter: true,
        enableSorting: true,
        columnDefs: [
            { name:'id', field: 'id', width: '10%'},
            { name:'titulo', field: 'title', width: '30%'},
            { name:'lanzamiento', field: 'release_date' },
            { name:'popularidad', field: 'popularity', aggregationType: uiGridConstants.aggregationTypes.sum, aggregationHideLabel: true, footerCellFilter: 'number:2'},
            { name:'valoracion', field: 'vote_average', width: '10%', aggregationType: uiGridConstants.aggregationTypes.sum, aggregationHideLabel: true, footerCellFilter: 'number:2'},
            { name:'generos id', field: 'get_genre'},
            { name:'ganancias', field: 'popularity', cellFilter: 'currency', footerCellFilter: 'currency', aggregationType: uiGridConstants.aggregationTypes.sum, aggregationHideLabel: true,},
            { name:'descripcion', field: 'description', cellTemplate:'<button style="width: 70px; height: 30px; font-size: 13px" data-toggle="modal" data-target="#modalDescription" class="btn btn-info" ng-click="grid.appScope.showDescription(row.entity.id)">Ver</button>', cellClass: 'text-center'}
        ],
        rowHeight: '35px',
        onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
        }
    };

    $scope.gridOptions.onRegisterApi = function(gridApi){
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
            $scope.rowsSelected = $scope.gridApi.selection.getSelectedRows();
            $scope.countRows = $scope.rowsSelected.length;
            if ($scope.countRows > 0){
                $scope.id = row.entity.id;
                $scope.titulo = row.entity.title;
                $scope.popularidad = row.entity.popularity;
                $scope.valoracion = row.entity.vote_average;
                $scope.generos = row.entity.get_genre;
                $scope.ganancias = row.entity.popularity;
            }
        });
    };
    
    $scope.showDescription = function(elem_Id){
        var obj_results = $scope.gridOptions.data;
        for (var i in obj_results){
            if (obj_results[i].id === elem_Id){
                $scope.descriptionTitle = obj_results[i].title;
                $scope.descriptionContent = obj_results[i].overview;
            }
        }
    };

    $scope.checkData = function(){
        var obj_results = $scope.gridOptions.data;
        for (var i in obj_results){
            if (obj_results[i].id == $scope.id){
                $scope.checkDataRes = 1;
                $scope.positionValue = i;
                break;
            }
        }
    };

    $scope.unSelectAll = function(){
        $scope.gridApi.selection.clearSelectedRows();
    }

    $scope.addData = function() {
        $scope.checkData();
        var resCheck = $scope.checkDataRes;
        if (resCheck == 1){
            $ngConfirm({
                title: '¡Datos duplicados!',
                content: '¿Continuar sobreescribiendo los datos?',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                        $scope.clearData();
                        $('#btnClose').trigger('click');
                    },
                    save: {
                        text: 'Guardar cambios',
                        btnClass: 'btn-red',
                        action: function(){
                            $scope.putOnData();       
                        }
                    }
                }
            });
        } else {
            $scope.putOnData();
        }
    };
    
    $scope.putOnData = function(){
        
        if ($scope.positionValue >= 0){

            $scope.gridOptions.data[$scope.positionValue] = {
                'id': $scope.id,
                'title': $scope.titulo,
                'release_date': $filter('formatDate')($scope.lanzamiento),
                'popularity': $scope.popularidad,
                'vote_average': $scope.valoracion,
                'get_genre': $scope.generos,
                'popularity': $scope.ganancias
            };

        } else {

            $scope.gridOptions.data.unshift({
                'id': $scope.id,
                'title': $scope.titulo,
                'release_date': $filter('formatDate')($scope.lanzamiento),
                'popularity': $scope.popularidad,
                'vote_average': $scope.valoracion,
                'get_genre': $scope.generos,
                'popularity': $scope.ganancias
            });

        }
        
        $('#modalAlert_1').hide('fade');
        $('#modalAlert_2').show('fade');
        $scope.clearData();
    };

    $scope.deleteData = function(){
        var position = 0;
        var obj_results = $scope.gridOptions.data;
        for (var i in obj_results){
            if (obj_results[i].id == $scope.id){
                positionValue = i;
                break;
            }
        }

        $ngConfirm({
            title: '¡Cuidado!',
            content: '¿Continuar eliminando los datos?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                close: function () {
                    $scope.clearData();
                },
                save: {
                    text: 'Continuar',
                    btnClass: 'btn-red',
                    action: function(){
                        $scope.gridOptions.data.splice(positionValue,1); 
                        $scope.clearData();
                    }
                }
            }
        });

    };

    $scope.clearData = function(){
        $('#btnClose').trigger('click');
        $scope.positionValue = -1;
        $('#modalAlert_1').hide('fade');
        $('#modalAlert_2').hide('fade');
        $scope.countRows = 0;
        $scope.editing = 0;
        $scope.checkDataRes = 0;
        $scope.id = "";
        $scope.titulo = "";
        $scope.lanzamiento = "";
        $scope.popularidad = "";
        $scope.valoracion = "";
        $scope.generos = "";
        $scope.ganancias = "";
        $scope.unSelectAll();  
    };

    $http(
    {
        method: 'GET',
        url: 'https://api.themoviedb.org/3/movie/top_rated?api_key=6e2adbaf416dc3211e330f1c47e04cfe&language=en-US&page=1'
        
    })
    .then(
        function (response){
            for(var i in response.data.results){
                var object_genre = response.data.results[i].genre_ids;
                var array_genre = [];
                for(var j in object_genre){
                    array_genre.push(object_genre[j]);
                }
                var result = object_genre.join(",");
                
                response.data.results[i].get_genre = result;
            }
            console.log(response.data.results);
            $scope.gridOptions.data = response.data.results;
        },
        function (error){
            console.log("Error: " + error);
        }
    );

});

app.filter('formatDate', function() {
    return function(x) {
        var res = x.toISOString().slice(0,10);
        return res;
    };
});

app.filter('toBootstrapDate', function() {
    return function(x) {
        var array = x.split("-");
        array.reverse();
        var result = array.join("/");
        return result;
    };
});

/*     **************************************** */
/*     **************************************** */


